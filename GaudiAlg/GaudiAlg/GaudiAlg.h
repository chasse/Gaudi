#ifndef GAUDIALG_GAUDIALG_H
#define GAUDIALG_GAUDIALG_H 1
// ============================================================================
// Include files
// ============================================================================
// Gaudi
// ============================================================================
// ============================================================================
// STD & STL
// ============================================================================
// ============================================================================

/** @namespace GaudiAlg GaudiAlg.h GaudiAlg/GaudiAlg.h
 *  Namespace with definition of useful constants, types and function,
 *  common for all classes from GaudiAlg package
 *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
 *  @date   2005-08-06
 */
namespace GaudiAlg {} // end of namespace GaudiAlg

#endif // GAUDIALG_GAUDIALG_H
